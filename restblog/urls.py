from django.conf.urls import patterns, include, url
from django.contrib import admin
from rest_framework import routers
from blog import views
from blog.views import UserPostList

admin.autodiscover()

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'posts', views.PostViewSet)

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'restblog.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin-dashboard/', include(admin.site.urls)),
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^(?P<username>[\w\-]+)/posts/$', UserPostList.as_view(), name='userpost-list')
)
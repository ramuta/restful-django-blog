from django.contrib.auth.models import User
from rest_framework import serializers
from blog.models import Post


class UserSerializer(serializers.HyperlinkedModelSerializer):
    posts = serializers.HyperlinkedIdentityField('posts', view_name='userpost-list', lookup_field='username')

    class Meta:
        model = User
        fields = ('url', 'username', 'email')


class PostSerializer(serializers.HyperlinkedModelSerializer):
    author = UserSerializer(required=False)

    class Meta:
        model = Post